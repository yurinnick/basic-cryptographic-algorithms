# coding: utf8

class CaesarChipperEncryption:

	def __init__(self, key):
		self.alphabet = str("абвгдеёжзийклмнопрстуфхцчшщъьэюя").decode('utf8')
		self.alphabet_length = len(self.alphabet)
		self.keysum = self.__generate_keysum(key.decode('utf8'))

	def __generate_keysum(self, key):
		keysum = 0
		for char in key:
			n = self.alphabet.find(char)
			if n != -1 :
				keysum += n
		return keysum

	def encrypt_file(self, path):
		input_file = open(path, "r")
		inputstr = input_file.read().decode('utf8')
		input_file.close()
		
		encrypted = ""
		for char in inputstr:
			char_pos = self.alphabet.find(char)
			if char_pos != -1:
				newchar = self.alphabet[(char_pos + self.keysum) % self.alphabet_length]
				encrypted += newchar
			else:
				encrypted += char

		return encrypted

	def decrypt_file(self, path):
		encrypted_input = open(path, "r")
		encryptedstr = encrypted_input.read().decode('utf8')
		encrypted_input.close()

		decrypted = ""
		for char in encryptedstr:
			char_pos = self.alphabet.find(char)
			if char_pos != -1:
				newchar = self.alphabet[(char_pos - self.keysum + self.alphabet_length) % self.alphabet_length]
				decrypted += newchar
			else:
				decrypted += char
		return decrypted


#Class work example
# cc = CaesarChipper("пароль")

# encrypted_file = open("encrypted.txt", "w+")
# encryptedstr = cc.encrypt_file("input.txt")
# encrypted_file.write(encryptedstr.encode('utf8'))
# encrypted_file.close()

# decrypted_file = open("decrypted.txt", "w+")
# decryptedstr = cc.decrypt_file("encrypted.txt")
# decrypted_file.write(decryptedstr.encode('utf8'))
# decrypted_file.close()