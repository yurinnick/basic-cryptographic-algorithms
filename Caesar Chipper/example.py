# coding: utf8

from CaesarChipper import *

cc = CaesarChipperEncryption("пароль")

encrypted_file = open("encrypted.txt", "w+")
encryptedstr = cc.encrypt_file("input.txt")
encrypted_file.write(encryptedstr.encode('utf8'))
encrypted_file.close()

decrypted_file = open("decrypted.txt", "w+")
decryptedstr = cc.decrypt_file("encrypted.txt")
decrypted_file.write(decryptedstr.encode('utf8'))
decrypted_file.close()